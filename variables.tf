variable "name" {
  description = "Name"
  type        = string
}

variable "kms_cmk_bakery_arn" {
  description = "KMS CMK Bakery ARN - set to your environments CMK outside bakery"
  type        = string
}

variable "push_pull_trusted_roles" {
  description = "List of accounts to trust to pull the image"
  type        = list
}

variable "OrgID" {
  description = "KMS CMK Bakery ARN - set to your environments CMK outside bakery"
  type        = string
}