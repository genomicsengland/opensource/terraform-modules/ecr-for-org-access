variable "name" {
  description = "Name"
  type        = string
  default     = "<<your ecr name here>>"
}

variable "kms_cmk_bakery_arn" {
  description = "KMS CMK Bakery ARN - set to your environments CMK outside bakery"
  type        = string
  default = "<< you can take the kms key from bakery account or contact CE team for the right credentials>>"
}

variable "push_pull_trusted_roles" {
  description = "List of accounts to trust to pull the image"
  type        = list
  default = ["<< the role that will be needed to deploy to the needed environment. Ideally bakery >>"]
}

variable "OrgID" {
  description = "Org ID required for the access of all trusted accounts in the org"
  type        = string
  default = "<< org id >>"
}

variable "region" {
  description = "region"
  type        = string
  default     = "eu-west-2"
}