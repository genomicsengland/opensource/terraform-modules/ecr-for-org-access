module "ecr_creation" {
  source = "../"
  name                 = "my_ecr_name"
  kms_cmk_bakery_arn         = var.kms_cmk_bakery_arn
  OrgID           = var.OrgID
  push_pull_trusted_roles = var.push_pull_trusted_roles
}