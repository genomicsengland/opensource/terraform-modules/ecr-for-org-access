# ECR For Org Access

ECR repository creation to share an image with all of the org accounts

## Getting started

This module is designed to allow the teams to easily create ECR repositories and push the images to the common account repo that will allow every single additional account to access the required images.


## Pulling the module

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:35726d1e49b693b7bc041fc5bc15bd04?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:35726d1e49b693b7bc041fc5bc15bd04?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:35726d1e49b693b7bc041fc5bc15bd04?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/genomicsengland/opensource/terraform-modules/ecr-for-org-access.git
git branch -M main
git push -uf origin main
```

## EXAMPLE MODULE CODE

In the example/ folder you will find the sample code that can be used to run the module. All of the sensitive variables have been removed for the security reasons.

```
ecr-for-org-access % tree example
example
├── main.tf
├── provider.tf
├── variables.tf
└── vars
    └── prod.tfvars

```

### Outputs

We ar also providing the teams with 3 outputs from the repository creation that could be used to call the necessary arn variables from ecr in the outputs.tf file

```
output "arn" {
  value = aws_ecr_repository.ecr_repository.arn
  description = "ECR Repository ARN"
}

output "registryId" {
  value = aws_ecr_repository.ecr_repository.registry_id
  description = "ECR Repository Registry Id"
}

output "url" {
  value = aws_ecr_repository.ecr_repository.repository_url
  description = "ECR Repository URL"
}
```

## Run the Module

```
module "ecr_creation" {
  source                    = "../"
  name                      = "my_ecr_name"
  kms_cmk_bakery_arn        = var.kms_cmk_bakery_arn
  OrgID                     = var.OrgID
  push_pull_trusted_roles   = var.push_pull_trusted_roles
}
```
## Expected Output

```
  # module.ecr_creation.aws_ecr_lifecycle_policy.ecr_lifecycle_policy will be created
  + resource "aws_ecr_lifecycle_policy" "ecr_lifecycle_policy" {
      + id          = (known after apply)
      + policy      = jsonencode(
            {
              + rules = [
                  + {
                      + action       = {
                          + type = "expire"
                        }
                      + description  = "Keep last 30 images"
                      + rulePriority = 1
                      + selection    = {
                          + countNumber   = 30
                          + countType     = "imageCountMoreThan"
                          + tagPrefixList = [
                              + "v",
                            ]
                          + tagStatus     = "tagged"
                        }
                    },
                ]
            }
        )
      + registry_id = (known after apply)
      + repository  = "MyECRName"
    }

  # module.ecr_creation.aws_ecr_repository.ecr_repository will be created
  + resource "aws_ecr_repository" "ecr_repository" {
      + arn                  = (known after apply)
      + id                   = (known after apply)
      + image_tag_mutability = "IMMUTABLE"
      + name                 = "MyECRName"
      + registry_id          = (known after apply)
      + repository_url       = (known after apply)
      + tags_all             = (known after apply)

      + encryption_configuration {
          + encryption_type = "KMS"
          + kms_key         = "arn:aws:kms:eu-west-2:<<ACCOUNT REMOVED FOR THE SECURITY REASONS>>:key/<< KEY REMOVED FOR THE SECURITY REASONS>>"
        }

      + image_scanning_configuration {
          + scan_on_push = true
        }
    }

  # module.ecr_creation.aws_ecr_repository_policy.ecr_repository_policy will be created
  + resource "aws_ecr_repository_policy" "ecr_repository_policy" {
      + id          = (known after apply)
      + policy      = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = [
                          + "ecr:ListImages",
                          + "ecr:GetDownloadUrlForLayer",
                          + "ecr:BatchGetImage",
                        ]
                      + Condition = {
                          + StringEquals = {
                              + aws:PrincipalOrgID = [
                                  + "<<ORG ID REMOVED FOR SECURITY REASONS>>",
                                ]
                            }
                        }
                      + Effect    = "Allow"
                      + Principal = {
                          + AWS = "*"
                        }
                      + Sid       = ""
                    },
                  + {
                      + Action    = [
                          + "ecr:UploadLayerPart",
                          + "ecr:PutImage",
                          + "ecr:ListImages",
                          + "ecr:InitiateLayerUpload",
                          + "ecr:GetLifecyclePolicy",
                          + "ecr:GetDownloadUrlForLayer",
                          + "ecr:DescribeRepositories",
                          + "ecr:CompleteLayerUpload",
                          + "ecr:BatchGetImage",
                          + "ecr:BatchCheckLayerAvailability",
                        ]
                      + Effect    = "Allow"
                      + Principal = {
                          + AWS = "arn:aws:iam::<<ACCOUNT REMOVED FOR THE SECURITY REASONS>>:role/aws-reserved/sso.amazonaws.com/eu-west-2/AWSReservedSSO_AdministratorAccess_04451552d2d523e0"
                        }
                      + Sid       = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + registry_id = (known after apply)
      + repository  = "MyECRName"
    }

```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| name | The Name of the ECR repository | `string` | `{}` | yes |
| kms_cmk_bakery_arn | KMS key for the Bakery Prod account | `string` | `{}` | yes |
| OrgID | Org for prod OR test accounts | `string` | `{}` | yes |
| push_pull_trusted_roles | Role for the account ECR repo is deployed to | `string` | `{}` | yes |



## Outputs

| Name | Description |
|------|-------------|
| arn | ECR Repository ARN |
| registryId | ECR Repository Registry Id |
| url | ECR Repository URL |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->