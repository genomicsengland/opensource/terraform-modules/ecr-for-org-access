resource "aws_ecr_repository" "ecr_repository" {
  name                 = var.name
  image_tag_mutability = "IMMUTABLE"

  encryption_configuration {
    encryption_type = "KMS"
    kms_key         = var.kms_cmk_bakery_arn
  }

  image_scanning_configuration {
    scan_on_push = true
  }
}